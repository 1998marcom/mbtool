# mbtool

mbtool is a simple program to partly overcome rclone/g-drive limitations: Google does allow only a rather small number of files upload per second, though ther is no limitation on bandwith. mbtool exploit this to backup files and folders tar-red together. It supports pseudo-incremental backups (it only tars and uploads folder that have been modified after the time given, and only the things that have been modified go into the tar).
At first **you should install and configure rclone**: here is a useful guide http://wiki.linuxquestions.org/wiki/Rsync_with_Google_Drive
Then keep the remote name you chose for drive. You can **then clone this repo and configure mbtool** open mbtool.hpp and modify as you want lines 20-22. In line 21 you should write your remote name in place of "DriveSNS". In line 21 write the defalut root folder. Then just open command line and type "make". Enter you admin password and you're done.

Usage is **mbtool [-t[UNIXtime]] source [-r] dest**

Files will be stored at the path defined by RootFolder/dest (where "dest" is what you enter as last argument on command line) unless you specify "-r" before dest, in which case the destination folder is simply dest.

Planned expansions:
* A python deamon to auto-backup incrementally all pc (e.g. once a day), and maybe also have a full backup once every while (e.g. once every two months).
* A feature to ripristinate backups somewhere by summing up all the previous incremental backups since last one.

