#ifndef MBTOOL_HEADER
#define MBTOOL_HEADER

#include <string>
#include <array>
#include <memory>
#include <stdexcept>
#include <cstring>
#include <vector>
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h> //può sempre servire
#include <unistd.h> //anche questo
#include <limits.h> //questo l'ho usato veramente

#define DEFAULT_PATH "/Backups/Mint17_Acer5742G"
#define DriveName "DriveSNS"
#define MAX_TAR_SIZE 30000000 //if possible have files less than 30MB in size
#define MAX_PATH 4096

#define IS_TIME(x) (!incremental||x>=tiempo)

//ROOT & DriveName should be passed by makefile

using namespace std;

bool root, incremental;
time_t tiempo;

void exec(const char* cmd) {
	array<char, 128> buffer;
	string result;
	unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"), pclose);
	if (!pipe) {
		throw runtime_error("popen() failed!");
	}
	while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
		cout<< buffer.data();
	}
}

string adjust_for_terminal(const string & input);

void drivesave(const string & file, const string & path="/");

void driveload(const string & file, const string & path="/");

class file_t {
	public:
		void drivesave_this(const string & path);
		unsigned long long my_size;
		string my_path;
		time_t my_date;
		file_t(string given_path);
		file_t();
};

class dir_t {
	public:
		void drivesave_this(const string & path);
		unsigned long long my_size;
		string my_path;
		time_t my_date;
		
		void drivesave_rec(const string & path);
		unsigned int N_files;
		vector<dir_t> dirvec;
		vector<file_t> filevec;
		dir_t(string given_path);
		dir_t();
};
#endif
