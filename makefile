
all: locate

locate: compile
	sudo mv ./mbtool /usr/bin/ 

compile: mbtool.cpp mbtool.hpp
	g++ mbtool.cpp -o mbtool -std=gnu++11
clean: 
	sudo rm /usr/bin/mbtool
