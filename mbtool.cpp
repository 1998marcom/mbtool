#include "mbtool.hpp"

//ROOT & DriveName should be passed by makefile
using namespace std;

string adjust_for_terminal(const string & input) {
	string ret;
	ret= input;
	size_t index = 0;
	while (true) {
		/* Locate the substring to replace. */
		index = ret.find(" ", index);
		if (index >= ret.size()) break;
		if (ret[index-1]=='\\') {
			index++;
			continue;
		}
		/* Make the replacement. */
		ret.replace(index, 1, "\\ ");
	
		/* Advance index forward so the next iteration doesn't pick it up as well. */
		index += 2;
	}
	return ret;
}

void file_t::drivesave_this(const string & path) {
	if ( IS_TIME(my_date) )
		drivesave(my_path,path);
}

file_t::file_t(string given_path) {
	my_path=given_path;
	struct stat tempstat;
	stat(my_path.c_str(), &tempstat);
	my_date=tempstat.st_mtime;
	my_size=tempstat.st_size;
}

file_t::file_t() {
	cout<<"o coglio'"<<endl;
	exit(42);
}

void dir_t::drivesave_this(const string & path) {
	if (IS_TIME(my_date))
		drivesave(my_path,path);
}

void dir_t::drivesave_rec(const string & path) {
	if (my_size<MAX_TAR_SIZE) {
		cout<<"1"<<endl;
		string dest;
		dest = path;// + my_path.substr(my_path.find_last_of('/'));
		drivesave_this(dest);
	} else {
		cout<<"2"<<my_path<<endl;
		string command;
		command = "tar -cvzf "+adjust_for_terminal(my_path)+".tar.gz";
		unsigned int i;
		unsigned long long int maxsize = MAX_TAR_SIZE/N_files;
		cout<<maxsize<<endl;
		for (i=0;i<dirvec.size();i++) {
			if (dirvec[i].my_size<=maxsize && IS_TIME(dirvec[i].my_date) )
				command+= " "+adjust_for_terminal(dirvec[i].my_path);
			else if (IS_TIME(dirvec[i].my_date))
				dirvec[i].drivesave_rec(path+"/"+my_path.substr(my_path.find_last_of('/')+1));
		}
		for (i=0;i<filevec.size();i++) {
			if (filevec[i].my_size<=maxsize && IS_TIME(filevec[i].my_date))
				command+= " "+adjust_for_terminal(filevec[i].my_path);
			else if ( IS_TIME(filevec[i].my_date) )
				filevec[i].drivesave_this(path+"/"+my_path.substr(my_path.find_last_of('/')+1));
		}
		cout<<"3"<<endl;
		cout<<command<<endl;
		exec(command.c_str());
		command = adjust_for_terminal(my_path)+".tar.gz";
		driveload(command,path);
		command = "rm -f "+adjust_for_terminal(my_path)+".tar.gz";
		cout<<command<<endl;
		system(command.c_str());
		cout<<"Uploaded big dir: "<<my_size<<"B : "<<my_path<<endl;
	}
}

dir_t::dir_t(string given_path) {
	//file standard
	cout<<"not even here"<<endl;
	my_path=given_path;
	struct stat tempstat;
	stat(my_path.c_str(), &tempstat);
	my_date=tempstat.st_mtime;
	my_size=tempstat.st_size;
	//dir things
	
		N_files=0;
		DIR* thisDIR;
		thisDIR=opendir(my_path.c_str());
		struct dirent* entpt;
		while ( (entpt = readdir(thisDIR)) != NULL ) {
			string filename(entpt->d_name);
			if (filename == ".." or filename == ".") continue;
			string fullpath(my_path+"/"+filename);
			cout<<fullpath<<endl;
			if (entpt->d_type == DT_DIR) {
				dir_t tempdir(fullpath);
				dirvec.push_back(tempdir);
				my_size += dirvec.back().my_size;
			} else {
				file_t tempfile(fullpath);
				filevec.push_back(tempfile);
				my_size += filevec.back().my_size;
			}
			N_files++;
		}
		cout<<"Examinated dir: "<<my_path<<" of size "<<my_size<<"B.\n";
		closedir(thisDIR);
	
}

dir_t::dir_t() {
	cout<<"o coglio' 2"<<endl;
	exit(42);
}

void drivesave(const string & file, const string & path) {
	string command; //service string to store commands
	//tar
	command = "tar -cvzf ";
	command+= adjust_for_terminal(file) + ".tar.gz" + " ";
	command+= adjust_for_terminal(file);
	cout<<command<<endl;
	exec(command.c_str());
	//driveload
	command = adjust_for_terminal(file)+".tar.gz";
	driveload(command,path);
	//remove tar
	command = "rm -f "+adjust_for_terminal(file)+".tar.gz";
	cout<<command<<endl;
	system(command.c_str());
}

void driveload(const string & file, const string & path) {
	string dest;
	if (!root) { //if not starting from "/" but from DEFAULT_PATH
		dest=DEFAULT_PATH;
		if (dest.empty() && path.empty()) {
			dest = "/";
		} else if (dest.empty() && !path.empty()) {
			dest = path;
		} else if (!dest.empty() && path.empty())
			; // dest = dest
		  else if (!dest.empty() && !dest.empty()) {
			if (dest.back()=='/' && path.front()=='/')
				dest=dest+path.substr(1);
			else if (!(dest.back()=='/') && !(path.front()=='/'))
				dest=dest+"/"+path;
			else
				dest=dest+path;
		}
	} else if (root) { //if starting from "/"
		if (path.empty())
			dest="/";
		else
			dest=path;
	} else { //should never be reached
		cout<<"PORCA PUTTANA"<<endl;
		exit(42);
	}
	
	if (dest.front()!='/')
		dest.insert(dest.begin(),'/');
	string command;
	command = "rclone copy -v "+adjust_for_terminal(file)+" "+adjust_for_terminal(DriveName)+":"+adjust_for_terminal(dest);
	cout<<command<<endl;
	exec(command.c_str());
}


//usage should be mbtool [sourcefolder] [-r/-root][destfolder]
//if sourcefolder is omitted, we are referring to the current folder
//if destfolder is omitted, default folder is used
int main(int argc, char** argv) {
	string start, end;
	root = false;
	incremental = false;
	unsigned int i;
	for (i=1;i<argc;i++) {
		string timstr = argv[i];
		if (timstr.substr(0,2) == "-t"&&incremental==false) {
			incremental = true;
			tiempo = stoi (timstr.substr(2));
			argc--;
			argv[i]=argv[i+1];
		} else if (incremental == true) {
			argv[i]==argv[i+1];
		}
	}
	cout<<argv[2]<<endl;
	if (argc>2) {
		if ( !strcmp(argv[2],"-r") || !strcmp(argv[2],"-root") ) {
			root =true;
			argv[2]=argv[3]; //Whoo che porcata !!!
		} else if (argc>3)
			exit(43);
		end=argv[2]; //grazie alla porcata metto argv[2] e chissene
	}
	char abs_path[MAX_PATH];
	start=realpath(argv[1],abs_path);
	cout<<"got here"<<endl;
	struct stat file_stat;
	stat(start.c_str(),&file_stat);
	if (S_ISDIR(file_stat.st_mode)) {
		dir_t maindir(start);
		cout<<"REquired tree has been built"<<endl;
		sleep(5);
		maindir.drivesave_rec(end);
	} else {
		cout<<"not here"<<endl;
		file_t file(start);
		cout <<"File found"<<endl;
		sleep(5);
		file.drivesave_this(end);
	}
	cout<<"OK?"<<endl;
	return 0;
}
